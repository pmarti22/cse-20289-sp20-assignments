#!/bin/sh

# Globals

# Website Url
URL="https://forecast.weather.gov/zipcity.php"

# Notre Dame Zipcode (Default)

ZIPCODE=46556

# If 0, no forecast. If 1, forecast required.
FORECAST=0

# If 0, fahrenhait. If 1, celsius
CELSIUS=0

# HTML for myforecast
HTML="myforecast-current"

# Functions

usage() {
	cat 1>&2 <<EOF
Usage: $(basename $0) [zipcode]

-c    Use Celsius degrees instead of Fahrenheit for temperature
-f    Display forecast text

If zipcode is not provided, then it defaults to $ZIPCODE.
EOF
	exit $1
}

weather_information() {
	# Fetch weather information from URL based on ZIPCODE
	curl -sL $URL?inputstring=$ZIPCODE
}

temperature() {
	# Extract temperature information from weather source
	if [ $CELSIUS = 0 ]; then
		weather_information | grep $HTML"-lrg" |grep -Eo '[0-9]+' 
	else
		weather_information | grep $HTML"-sm" | grep -E '[0-9]+' | cut -d '>' -f 2 | cut -d '&' -f 1
	fi
}

forecast() {
	# Extract forecast information from weather source
	weather_information | grep "myforecast-current" | head -n 1 | cut -d '>' -f 2 | cut -d '<' -f 1 
}

# Parse Command Line Options

while [ $# -gt 0 ]; do
	case $1 in
		-h) 
			usage 0;;
		-c)
			CELSIUS=1;;
		-f)
			FORECAST=1;;
		*)
			ZIPCODE=$1;;
	esac
shift
done

#Display Information

if [ $FORECAST = 1 ]; then
	echo "Forecast:    $(forecast)"
fi
echo "Temperature: $(temperature) degrees"
