#!/bin/sh

# Globals
CITY=""
STATE="INDIANA"
URL=https://www.zipcodestogo.com/

# Functions

usage() {
	cat 1>&2 <<EOF
Usage: $(basename $0)

-c    CITY to search for	
-s    STATE to search for

If no CITY is specified, return all the zip codes in the STATE
If no STATE is specified, should assume the STATE is "Indiana".
EOF
	exit $1
}

# Parse Command Line Options

while [ $# -gt 0 ]; do
	case $1 in
	-h) usage 0;;
	-c) shift;
		CITY=$1;;
	-s) shift;
		STATE=`echo $1 | sed 's/ /%20/g'`;;
	*) usage 1;;
	esac
	shift
done

# Filter Pipeline(s)

if [ "$CITY" = "" ]; then
	curl -s $URL$STATE/ | grep -Eo "/.*/[0-9]{5}" | cut -d '/' -f 6
else
	curl -s $URL$STATE/ | grep -Eo "$CITY/.*/[0-9]{5}" | cut -d '/' -f 3
fi
