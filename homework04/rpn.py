#!/usr/bin/env python3

import os
import sys

# Globals

OPERATORS = {'+', '-', '*', '/'}

# Functions

def usage(status=0):
    ''' Display usage message and exit with status. '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname}

By default, {progname} will process expressions from standard input.''')

    sys.exit(status)

def error(message):
    ''' Display error message and exit with error. '''
    print(message, file=sys.stderr)
    sys.exit(1)

def evaluate_operation(operation, operand1, operand2):
    ''' Return the result of evaluating the operation with operand1 and
    operand2.

    >>> evaluate_operation('+', 4, 2)
    6

    >>> evaluate_operation('-', 4, 2)
    2

    >>> evaluate_operation('*', 4, 2)
    8

    >>> evaluate_operation('/', 4, 2)
    2.0
    '''
    # Evaluate depending on operator
    if operation == '+':
        return operand1 + operand2
    elif operation == '-':
        return operand1 - operand2
    elif operation == '*':
        return operand1 * operand2
    elif operation == '/':
        return operand1 / operand2


def evaluate_expression(expression):
    ''' Return the result of evaluating the RPN expression.

    >>> evaluate_expression('4 2 +')
    6.0

    >>> evaluate_expression('4 2 -')
    2.0

    >>> evaluate_expression('4 2 *')
    8.0

    >>> evaluate_expression('4 2 /')
    2.0

    >>> evaluate_expression('4 +')
    Traceback (most recent call last):
    ...
    SystemExit: 1

    >>> evaluate_expression('a b +')
    Traceback (most recent call last):
    ...
    SystemExit: 1
    '''

    # Initialize an empty list
    list = []
    expression = expression.rstrip()
    expression = expression.split()

    for character in expression:
        list.append(character)

        # Iterate through elements and pop last elements and apply the operation
        # Iterate until all elements are done
        for char in list:
            if char in OPERATORS:
                if len(list) > 2:
                    try:
                        operation = list.pop()
                        operand2 = float(list.pop())
                        operand1 = float(list.pop())
                        list.append(float(evaluate_operation(operation, operand1, operand2)))
                    except ValueError:
                        error('Invalid Input. Unable to convert operands to float')
                else:
                    error('Not Enough Inputs')

    # Return value when there is only one item in list
    if len(list) == 1:
        try:
            return float(list[0])
        except ValueError:
            error(f'Could not print {list[0]} into float')

def main():
    ''' Parse command line arguments and process expressions from standard
    input. '''

    # Command Line: sys.argv
    for line in sys.argv:
        if line == '-h':
            usage()
    # For operations. sys.stdin
    for line in sys.stdin:
        print(evaluate_expression(line))

# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
