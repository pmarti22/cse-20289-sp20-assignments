#!/usr/bin/env python3

import collections
import os
import sys

import csv
import requests

# Constants

URL     = 'https://yld.me/raw/axPa'
TAB     = ' '*8
GENDERS = ('M', 'F')
ETHNICS = ('B', 'C', 'N', 'O', 'S', 'T', 'U')

# Functions

def usage(status=0):
    ''' Display usage information and exit with specified status '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [options] [URL]

    -y  YEARS   Which years to display (default: all)
    -p          Display data as percentages.
    -G          Do not include gender information.
    -E          Do not include ethnic information.
    ''')
    sys.exit(status)

def load_demo_data(url=URL):
    ''' Load demographics from specified URL into dictionary

    >>> load_demo_data('https://yld.me/raw/ilG').keys()
    dict_keys(['2013', '2014', '2015', '2016', '2017', '2018', '2019'])

    >>> load_demo_data('https://yld.me/raw/ilG')['2013'] == \
            {'M': 1, 'B': 2, 'F': 1, 'TOTAL': 2}
    True

    >>> load_demo_data('https://yld.me/raw/ilG')['2019'] == \
            {'M': 1, 'U': 2, 'F': 1, 'TOTAL': 2}
    True
    '''
    # TODO: Request data from url and load it into dictionary organized in the
    # following fashion:
    #
    #   {'year': {'gender': count, 'ethnic': count, 'TOTAL': count}}

    request = requests.get(url).text.splitlines()
    request.pop(0)

    data = {}

    for line in csv.reader(request):
        if line[0] in data.keys():
            data[line[0]][line[1]] = data[line[0]].get(line[1], 0) + 1
            data[line[0]][line[2]] = data[line[0]].get(line[2], 0) + 1
        else:
            data[line[0]] = {}
            data[line[0]][line[1]] = 1
            data[line[0]][line[2]] = 1

    for k in data.keys():
        data[k]['TOTAL'] = data[k].get('M', 0) + data[k].get('F', 0)

    return data

def print_demo_separator(years, char='='):
    ''' Print demographics separator

    Note: The row consists of the 8 chars for each item in years + 1.

    >>> print_demo_separator(['2012', '2013'])
    ========================
    '''
    #  TODO: Print row of separator characters
    for i in range (8 * (len(years) + 1)):
        print(char, end='')
    print('')

def print_demo_years(years):
    ''' Print demographics years row

    Note: The row is prefixed by 4 spaces and each year is right aligned to 8
    spaces ({:>8}).

    >>> print_demo_years(['2012', '2013'])
            2012    2013
    '''
    # TODO: Print row of years
    print('    ', end='')
    for y in years:
        print(f"{y:>8}", end='')
    print('')

def print_demo_fields(data, years, fields, percent=False):
    ''' Print demographics information (for particular fields)

    Note: The first column should be a 4-spaced field name ({:>4}), followed by
    8-spaced right aligned data columns ({:>8}).  If `percent` is True, then
    display a percentage ({:>7.1f}%) rather than the raw count.

    >>> data  = load_demo_data('https://yld.me/raw/ilG')
    >>> years = sorted(data.keys())
    >>> print_demo_fields(data, years, GENDERS, False)
       M       1       1       1       1       1       1       1
       F       1       1       1       1       1       1       1
    '''
    # TODO: For each field, print out a row consisting of data from each year.
    if fields == GENDERS:
        for g in GENDERS:
            print(f"{g:>4}", end='')
            for y in years:
                if percent:
                    percentage = (data[y].get(g,0) / data[y]['TOTAL']) * 100
                    print(f"{percentage:>7.1f}%", end='')
                else:
                    print(f"{data[y].get(g,0):>8}", end='')
            print('')

    if fields == ETHNICS:
        for e in ETHNICS:
            print(f"{e:>4}", end='')
            for y in years:
                if percent:
                    percentage = (data[y].get(e,0) / data[y]['TOTAL']) * 100
                    print(f"{percentage:>7.1f}%", end='')
                else:
                    print(f"{data[y].get(e,0):>8}", end='')
            print('')

def print_demo_data(data, years=None, percent=False, gender=True, ethnic=True):
    ''' Print demographics data for the specified years and attributes '''
    # TODO: Verify the years parameter (if None then extract from data,
    # otherwise use what is given).  Ensure years is sorted.
    if not years:
        years = sorted(data.keys())
    else:
        years = csv.reader(years.splitlines())
        temp = []
        for y in years:
            temp.append(y)
        years = sorted(temp[0])

    # TODO: Print years header with separator
    print_demo_years(years)
    print_demo_separator(years)

    # TODO: Print gender and ethic data if enabled
    if gender:
        print_demo_gender(data, years, percent)
    if ethnic:
        print_demo_ethnic(data, years, percent)

def print_demo_gender(data, years, percent=False):
    ''' Print demographics gender information '''
    print_demo_fields(data, years, GENDERS, percent)
    print_demo_separator(years, '-')

def print_demo_ethnic(data, years, percent=False):
    ''' Print demographics ethnic information '''
    print_demo_fields(data, years, ETHNICS, percent)
    print_demo_separator(years, '-')

def main():
    ''' Parse command line arguments, load data from url, and then print
    demographic data. '''
    # TODO: Parse command line arguments
    arguments = sys.argv[1:]
    url       = URL
    years     = None
    gender    = True
    ethnic    = True
    percent   = False

    # TODO: Load data from url and then print demograpic data with specified
    # arguments
    while len(arguments) != 0:
        arg = arguments.pop(0)
        if arg == '-p':
            percent = True
        elif arg == '-G':
            gender = False
        elif arg == '-E':
            ethnic = False
        elif arg == '-y':
            years = arguments.pop(0)
        elif arg == '-h':
            usage()
        else:
            url = arg

    data = load_demo_data(url)

    print_demo_data(data, years, percent, gender, ethnic)

# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
