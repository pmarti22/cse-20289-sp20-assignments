#!/usr/bin/env python3

import concurrent.futures
import hashlib
import os
import string
import sys

# Constants

ALPHABET = string.ascii_lowercase + string.digits

# Functions

def usage(exit_code=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-a ALPHABET -c CORES -l LENGTH -p PATH -s HASHES]
    -a ALPHABET Alphabet to use in permutations
    -c CORES    CPU Cores to use
    -l LENGTH   Length of permutations
    -p PREFIX   Prefix for all permutations
    -s HASHES   Path of hashes file''')
    sys.exit(exit_code)

def md5sum(s):
    ''' Compute md5 digest for given string. '''
    # TODO: Use the hashlib library to produce the md5 hex digest of the given
    # string.
    return hashlib.md5(s.encode()).hexdigest()

def permutations(length, alphabet=ALPHABET):
    ''' Recursively yield all permutations of the given length using the
    provided alphabet. '''
    # TODO: Use yield to create a generator function that recursively produces
    # all the permutations of the given length using the provided alphabet.
    if length != 0:
        for i in alphabet:
            for j in permutations(length-1, alphabet):
                yield i + j
    else:
        yield ''

def flatten(sequence):
    ''' Flatten sequence of iterators. '''
    # TODO: Iterate through sequence and yield from each iterator in sequence.
    for i in sequence:
        yield from i

def crack(hashes, length, alphabet=ALPHABET, prefix=''):
    ''' Return all password permutations of specified length that are in hashes
    by sequentially trying all permutations. '''
    # TODO: Return list comprehension that iterates over a sequence of
    # candidate permutations and checks if the md5sum of each candidate is in
    # hashes.
    candPerm = []
    for i in permutations(length, alphabet):
        term = prefix + i
        if md5sum(term) in hashes:
            candPerm.append(term)
    return candPerm

def cracker(arguments):
    ''' Call the crack function with the specified arguments '''
    return crack(*arguments)

def smash(hashes, length, alphabet=ALPHABET, prefix='', cores=1):
    ''' Return all password permutations of specified length that are in hashes
    by concurrently subsets of permutations concurrently.
    '''
    # TODO: Create generator expression with arguments to pass to cracker and
    # then use ProcessPoolExecutor to apply cracker to all items in expression.
    arguments = ((hashes, length-1, alphabet, prefix+a) for a in alphabet)

    with concurrent.futures.ProcessPoolExecutor(cores) as executor:
        return flatten(executor.map(cracker, arguments))

def main():
    arguments   = sys.argv[1:]
    alphabet    = ALPHABET
    cores       = 1
    hashes_path = 'hashes.txt'
    length      = 1
    prefix      = ''

    # TODO: Parse command line arguments
    while arguments:
        flag = arguments.pop(0)

        if flag == '-a':
            try:
                alphabet = arguments.pop(0)
            except ValueError:
                print('Invalid Alphabet:')
                usage()

        elif flag == '-c':
            try:
                cores = int(arguments.pop(0))
            except ValueError:
                print('Number of cores must be an int.')
                usage()

        elif flag == '-l':
            try:
                length = int(arguments.pop(0))
            except ValueError:
                print('Length of permutations must be an int.')
                usage()

        elif flag == '-p':
            try:
                prefix = str(arguments.pop(0))
            except ValueError:
                print('Prefix must be able to be converted to a string.')
                usage()

        elif flag == '-s':
            try:
                hashes_path = arguments.pop(0)
            except ValueError:
                print('Path must be able to be converted to a string.')
                usage()

        elif flag == '-h':
            usage()


    # TODO: Load hashes set

    try:
        file = open(hashes_path).readlines()
        hashes = {''}
        for line in file:
            line = line.rstrip("\n")
            hashes.add(line)

    except IOError:
        print('Unknown file or directory for path to hashes file.')
        usage()

    # TODO: Execute crack or smash function
    results = smash(hashes, length, alphabet, prefix, cores)

    # TODO: Print all found passwords
    for i in results:
        print(i)

# Main Execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
