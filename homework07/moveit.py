#!/usr/bin/env python3

import atexit
import os
import sys
import tempfile

# Functions

def usage(status=0):
    ''' Display usage message for program and exit with specified status. '''
    print(f'Usage: {os.path.basename(sys.argv[0])} files...')
    sys.exit(status)

def save_files(files):
    ''' Save list of files to a temporary file and return the name of the
    temporary file. '''

    with tempfile.NamedTemporaryFile(delete=False) as tf:
        for file in files:
            fileB = str.encode(file+'\n')
            tf.write(fileB)
    return tf.name

def edit_files(path):
    ''' Fork a child process to edit the file specified by path using the user's
    default EDITOR (use "vim" if not set).  Parent waits for child process and
    returns whether or not the child was successful. '''

    editor = os.environ.get('EDITOR')
    if(editor == None):
        editor = 'vim'

    try:
        pid = os.fork()
    except OSError:
        print("Fork Error")
        sys.exit(1)

    if pid == 0:      # Child
        try:
            os.execlp(editor, editor, path)
        except (IndexError, OSError):
            print("Error")
            sys.exit(1)
    else:             # Parent
        pid, pid_wait = os.wait()
        return os.WEXITSTATUS(pid_wait)



def move_files(files, path):
    ''' For each file in files and the corresponding information from the file
    specified by path, rename the original source file to the new target path
    (if the names are different).  Return whether or not all files were
    successfully renamed. '''

    newFiles = open(path).read().strip().split("\n")
    zipped = zip(files, newFiles)
    for oldFile, newFile in zipped:
        if oldFile != newFile:
            try:
                os.rename(oldFile, newFile)
            except (IndexError, OSError):
                print("Error. Files Unsuccesfully Renamed")
                sys.exit(1)


def main():
    ''' Parse command line arguments, save arguments to temporary file, allow
    the user to edit the temporary file, move the files, and remove the
    temporary file. '''
    # TODO: Parse command line arguments
    arguments = sys.argv[1:]

    if len(arguments) == 0:
        usage()
    elif sys.argv[1] == '-h':
        usage()

    # TODO: Save files (arguments)
    tfName = save_files(arguments)

    # TODO: Register unlink to cleanup temporary file
    atexit.register(os.unlink, tfName)

    # TODO: Edit files stored in temporary file
    edit_files(tfName)

    # TODO: Move files stored in temporary file
    move_files(arguments, tfName)

# Main Execution

if __name__ == '__main__':
    main()
