#!/usr/bin/env python3

import os
import signal
import sys
import time

# Functions

def usage(status=0):
    ''' Display usage message for program and exit with specified status. '''
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [options] command...
Options:
    -t SECONDS  Timeout duration before killing command (default is 10)
''')
    sys.exit(status)

def error(message, status=1):
    ''' Display error message and exit with specified status. '''
    print(message, file=sys.stderr)
    sys.exit(status)

def alarm_handler(signum, frame):
    ''' Alarm handler that raises InterruptedError '''
    raise InterruptedError

def timeit(argv, timeout=10):
    ''' Run command specified by argv for at most timeout seconds.

    - After forking, the child executes the specified command.

    - After forking, the parent does the following:
        - Registers a handler for SIGALRM -
        - Set alarm for specified timeout -
        - Waits for child process -
        - If wait is interrupted, kills the child process and wait again
        - Prints total elapsed time
        - Exits with the status of the child process
    '''
    try:
        pid = os.fork()
    except OSError:
        sys.exit(1)

    if pid == 0:      # Child
        try:
            os.execvp(argv[0], argv)
        except (IndexError, OSError):
            sys.exit(1)
    else:             # Parent

        try:
            initTime = time.time()
            signal.signal(signal.SIGALRM, alarm_handler)
            signal.alarm(timeout)
            pid, pid_wait = os.wait()

        except InterruptedError:
            os.kill(pid, signal.SIGKILL)
            pid, pid_wait = os.wait()

        finalTime = time.time()
        timeTot = finalTime - initTime
        roundedTime = "{:.2f}".format(timeTot)
        print('Time Elapsed: '+str(roundedTime))

        if os.WIFEXITED(pid_wait):
            pid_wait = os.WEXITSTATUS(pid_wait)
        else:
            pid_wait = os.WTERMSIG(pid_wait)

        sys.exit(pid_wait)

def main():
    ''' Parse command line options and then execute timeit with specified
    command and timeout. '''

    arguments = sys.argv[1:]
    time = 10

    if len(arguments) == 0:
        usage(1)

    temp = arguments[0]
    if temp == '-t':
        arguments.pop(0)
        try:
            if len(arguments) > 0:
                time = int(arguments.pop(0))
        except ValueError:
            error('Value after -t must be integer')
    elif temp == '-h':
        usage(0)

    if len(arguments) == 0:
        usage(1)

    timeit(arguments, time)

# Main Execution

if __name__ == '__main__':
    main()
