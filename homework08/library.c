/* library.c: string utilities library */

#include "str.h"

#include <ctype.h>
#include <string.h>
#include <stdbool.h>

/**
 * Convert all characters in string to lowercase.
 * @param   s       String to convert
 **/
void	str_lower(char *s) {

  for (char *c = s; *c; c++) {
      *c = tolower(*c);
  }

}

/**
 * Convert all characters in string to uppercase.
 * @param   s       String to convert
 **/
void	str_upper(char *s) {

  for (char *c = s; *c; c++) {
    *c = toupper(*c);
  }

}

/**
 * Convert all characters in string to titlecase.
 * @param   s       String to convert
 **/
void str_title(char *s) {

  str_lower(s);
  *s = toupper(*s);

  bool title = false;
  for (char *c = s; *c; c++) {
    if(title && isalpha(*c)){
      *c = toupper(*c);
    }
    title = false;
    if(!(isalpha(*c))){
      title = true;
    }
  }

}

/**
 * Removes trailing newline (if present).
 * @param   s       String to modify
 **/
void    str_chomp(char *s) {

  char *tail = s + strlen(s) - 1;

  if(*tail == '\n')
    *tail = '\0';

}

/**
 * Removes whitespace from front and back of string (if present).
 * @param   s       String to modify
 **/
void    str_strip(char *s) {

  char *headR = s;

  while(*headR == ' '){
      headR++;
  }

  for(char *headW = s; *headW; headW++){
    *headW = *headR;
    headR++;
  }

  for (char *tail = s + strlen(s) - 1; *tail; tail--) {
    if(*tail == ' '){
        *tail = '\0';
    }
    else{
        break;
    }
  }

}

/**
 * Reverses a string.
 * @param   s       String to reverse
 **/
void    str_reverse(char *s) {

  char temp = *s;
  char *tail = s + strlen (s) - 1;

  for (char *head = s; head < tail; head++) {
      temp = *head;
      *head = *tail;
      *tail = temp;

      tail--;
    }

}

/**
 * Replaces all instances of 'from' in 's' with corresponding values in 'to'.
 * @param   s       String to translate
 * @param   from    String with letter to replace
 * @param   to      String with corresponding replacment values
 **/
void    str_translate(char *s, char *from, char *to) {

  int set[1<<8] = {0};
  int min = strlen(from);

  for(int i = 0; i < min; i++){
    set[(int)*(from+i)] = *(to+i);
  }

  for(char *c = s; *c; c++){
    if(set[(int)*c] != 0) {
      *c = set[(int)*c];
    }
  }

}

/**
 * Converts given string into an integer.
 * @param   s       String to convert
 * @param   base    Integer base
 * @return          Converted integer value
 **/
int	str_to_int(const char *s, int base) {

  int n = 0, p = 1;

  int size = strlen(s) - 1;

  for(int i = 0; i < size; i++) {
    p = p * base;
  }

  for(const char *head = s; *head; head++) {

        if(!(isalpha(*head))){
          n = n + p*((int)*head - 48);
        }
        else if((int)*head >= 65 && (int)*head <= 90){
          n = n + p*((int)*head - 55);
        }
        else if((int)*head >= 97 && (int)*head <= 122) {
          n = n + p*((int)*head - 87);
        }
        p = p / base;
    }

    return n;

}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
