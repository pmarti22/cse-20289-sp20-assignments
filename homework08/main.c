/* main.c: string library utility */

#include "str.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char *PROGRAM_NAME = NULL;

/* Flags */

enum {
    /* TODO*: Enumerate Flags */
    STRIP = 1 << 0,
    REVERSE = 1 << 1,
    LOWER = 1 << 2,
    UPPER = 1 << 3,
    TITLE = 1 << 4,
};

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s SOURCE TARGET\n\n", PROGRAM_NAME);
    fprintf(stderr, "Post Translation filters:\n\n");
    fprintf(stderr, "   -s      Strip whitespace\n");
    fprintf(stderr, "   -r      Reverse line\n");
    fprintf(stderr, "   -l      Convert to lowercase\n");
    fprintf(stderr, "   -u      Convert to uppercase\n");
    fprintf(stderr, "   -t      Convert to titlecase\n");
    exit(status);
}

void translate_stream(FILE *stream, char *source, char *target, int flag) {
    /* TODO: Process each line in stream by performing transformations */

    char buffer[BUFSIZ];

      while(fgets(buffer, BUFSIZ, stream))
      {
        str_chomp(buffer);

        if (source != NULL) {
          str_translate(buffer, source, target);
        }
        if (flag & STRIP) {
          str_strip(buffer);
        }
        if (flag & REVERSE) {
          str_reverse(buffer);
        }
        if (flag & LOWER) {
          str_lower(buffer);
        }
        if (flag & UPPER) {
          str_upper(buffer);
        }
        if (flag & TITLE) {
          str_title(buffer);
        }

        printf("%s\n", buffer);
      }

}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* TODO: Parse command line arguments */
    int argind = 1;
    int flag = 0;
    PROGRAM_NAME = argv[0];

    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
      char *arg = argv[argind++];
      if (strcmp(arg, "-h") == 0) {
        usage(0);
      } if (strcmp(arg, "-s") == 0){
        flag |= STRIP;
      } else if (strcmp(arg, "-r") == 0){
        flag |= REVERSE;
      } else if (strcmp(arg, "-l") == 0){
        flag |= LOWER;
      } else if (strcmp(arg, "-u") == 0){
        flag |= UPPER;
      } else if (strcmp(arg, "-t") == 0){
        flag |= TITLE;
      } else{
        usage(0);
      }
    }

    char *source = NULL, *target = NULL;

    if(argv[argind] != NULL) {
      source = argv[argind];
      target = argv[++argind];
    }

    /* TODO: Translate Stream */
    translate_stream(stdin, source, target, flag);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
