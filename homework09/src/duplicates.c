/* duplicates.c */

#include "hash.h"
#include "macros.h"
#include "table.h"

#include <dirent.h>
#include <limits.h>
#include <sys/stat.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Structures */

typedef struct {
    bool count;
    bool quiet;
} Options;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s paths...\n", PROGRAM_NAME);
    fprintf(stderr, "    -c     Only display total number of duplicates\n");
    fprintf(stderr, "    -q     Do not write anything (exit with 0 if duplicate found)\n");
    exit(status);
}

/**
 * Check if path is a directory.
 * @param       path        Path to check.
 * @return      true if Path is a directory, otherwise false.
 */
bool is_directory(const char *path) {

  // Template from StackOverflow
  struct stat s;
  stat(path, &s);

  if(!(stat(path,&s))){
    if( s.st_mode & S_IFDIR ) {
        return true;
    }
  }

  return false;
}

/**
 * Check if file is in table of checksums.
 *
 *  If quiet is true, then exit if file is in checksums table.
 *
 *  If count is false, then print duplicate association if file is in
 *  checksums table.
 *
 * @param       path        Path to file to check.
 * @param       checksums   Table of checksums.
 * @param       options     Options.
 * @return      0 if Path is not in checksums, otherwise 1.
 */
size_t check_file(const char *path, Table *checksums, Options *options) {

  char *hexdigest = calloc(1, HEX_DIGEST_LENGTH);
  hash_from_file(path, hexdigest);
  // printf("MD5sum: %s\n", hexdigest);

  Value *v = table_search(checksums, hexdigest);

  if(v != NULL){
    // printf("If: %s\n", path);
    if((*options).quiet){
      table_delete(checksums);
      free(options);
      free(hexdigest);
      exit(0);
    }
    else if (!((*options).count)){
      printf("%s is a duplicate of %s\n", path, (*v).string);
    }
    free(hexdigest);
    return 1;
  }
  else{
    // printf("Else: %s\n", path);
    Value n = {0};
    n.string = strdup(path);
    table_insert(checksums, hexdigest, n, STRING);
    free(n.string);
  }

  free(hexdigest);

  return 0;
}

/**
 * Check all entries in directory (recursively).
 * @param       root        Path to directory to check.
 * @param       checksums   Table of checksums.
 * @param       options     Options.
 * @return      Number of duplicate entries in directory.
 */
size_t check_directory(const char *root, Table *checksums, Options *options) {

  size_t count = 0;

  // Template from reading11
  DIR *d = opendir(root);
  if (!d) {
      return 0;
  }

  /* For each directory entry, check if it is a file, and print out the its
   * name and file size */
  for (struct dirent *e = readdir(d); e; e = readdir(d)) {
      /* Skip current directory and parent directory */
      if (streq(e->d_name, ".") || streq(e->d_name, "..")) {
          continue;
      }

      char path[BUFSIZ];
      path[0] ='\0';
      strcat(path, root);
      strcat(path, "/");
      strcat(path, e->d_name);

      if(!(is_directory(path))){
        count += check_file(path, checksums, options);
        }
      else{
        count += check_directory(path, checksums, options);
      }
  }

  /* Close directory handle */
  closedir(d);
  return count;
}

/* Main Execution */

int main(int argc, char *argv[]) {
    /* Parse command line arguments */
    int argind = 1;
    PROGRAM_NAME = argv[0];

    Options *op = calloc(1, sizeof(Options));
    (*op).count = false;
    (*op).quiet = false;

    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
      char *arg = argv[argind++];
      if (streq(arg, "-h")) {
        free(op);
        usage(0);
      } if (streq(arg, "-c")){
        (*op).count = true;
      } else if (streq(arg, "-q")){
        (*op).quiet = true;
      } else{
        free(op);
        usage(0);
      }
    }

    if(argv[argind] == NULL){
      free(op);
      usage(0);
    }

    /* Check each argument */
    size_t count = 0;
    Table *table = table_create(0);
    while(argv[argind] != NULL){
      if(!(is_directory(argv[argind]))){
        count += check_file(argv[argind], table, op);
      } else {
        count += check_directory(argv[argind], table, op);
      }
      argind++;
    }

    /* Display count if necessary */
    if((*op).count){
      printf("%zu\n", count);
    }

    if((*op).quiet){
      table_delete(table);
      free(op);
      exit(1);
    }

    // table_format(table, stdout);

    free(op);
    table_delete(table);


    return 0;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
