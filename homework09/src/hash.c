/* hash.c: hash functions */

#include "hash.h"

#include <stdio.h>


/**
 * Constants
 * http://isthe.com/chongo/tech/comp/fnv/
 */

#define FNV_OFFSET_BASIS    (0xcbf29ce484222325ULL)
#define FNV_PRIME           (0x100000001b3ULL)

/**
 * Compute FNV-1 hash.
 * @param   data            Data to hash.
 * @param   n               Number of bytes in data.
 * @return  Computed hash as 64-bit unsigned integer.
 */
uint64_t    hash_from_data(const void *data, size_t n) {
  uint64_t hash = FNV_OFFSET_BASIS;
  char *byte = (char*)data;
  for(int i = 0; i < n; i++){
    hash = hash ^ *(byte+i);
    hash = hash * FNV_PRIME;
  }
  return hash;
}

/**
 * Compute MD5 digest.
 * @param   path            Path to file to checksum.
 * @param   hexdigest       Where to store the computed checksum in hexadecimal.
 * @return  Whether or not the computation was successful.
 */
bool        hash_from_file(const char *path, char hexdigest[HEX_DIGEST_LENGTH]) {

  // Template from StackOverflow
  int n;
  MD5_CTX c;
  char buf[512];
  ssize_t bytes;
  unsigned char out[MD5_DIGEST_LENGTH];

  FILE * fp;
  fp = fopen(path, "r");

  if(fp == NULL){
    return false;
  }

  MD5_Init(&c);
  bytes=fread(buf, 1, 512, fp);
  while(bytes > 0)
  {
      MD5_Update(&c, buf, bytes);
      bytes=fread(buf, 1, 512, fp);
  }

  MD5_Final(out, &c);

  for(n = 0; n < MD5_DIGEST_LENGTH; n++)
      sprintf(hexdigest + n*2, "%02x", out[n]);
  printf("\n");

  fclose(fp);

  return true;

}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
