#!/bin/bash

q1_answer() {
    # TODO: Complete pipeline
	echo "All your base are belong to us" | tr a-z A-Z
}

q2_answer() {
    # TODO: Complete pipeline
	echo "monkeys love bananas" | sed 's/monkeys/gorillaz/g'
}

q3_answer() {
    # TODO: Complete pipeline
	echo "     monkeys love bananas" | sed 's/^ *//g'
}

q4_answer() {
    # TODO: Complete pipeline
	curl -sL https://yld.me/raw/yWh | grep ^root | cut -d ':' -f 7
}

q5_answer() {
    # TODO: Complete pipeline
	curl -sL https://yld.me/raw/yWh | sed -E -e 's-bin/bash|bin/csh|bin/tcsh-usr/bin/python-g' | grep python
}

q6_answer() {
    # TODO: Complete pipeline
	curl -sL https://yld.me/raw/yWh | grep -P '4([0-9]+)?7'
}
