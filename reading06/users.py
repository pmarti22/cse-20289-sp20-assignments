#!/usr/bin/env python3

import csv

# Constants

PATH = '/etc/passwd'

# TODO: Loop through ':' delimited data in PATH and extract the fifth field
# (user description)
file = open(PATH)
csvFile = csv.reader(file, delimiter=':')
csvList = list(csvFile)
file.close

temp = []

for r in csvList:
    if r[4]:
        temp.append(r[4])

# TODO: Print user descriptions in sorted order
temp.sort()
for i in temp:
    print(i)
