#!/usr/bin/env python3

import sys

def evens(stream):
	    # TODO: Implementation that uses yield statement
		for n in stream:
			n = n .strip()
			if int(n) % 2 == 0:
				yield n

print(' '.join(evens(sys.stdin)))
