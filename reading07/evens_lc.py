#!/usr/bin/env python3

import sys

print(' '.join(
	    # TODO: One-line expression with list comprehension
		[n.strip() for n in sys.stdin if not int(n) % 2]
		))
