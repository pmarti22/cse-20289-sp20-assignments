#!/usr/bin/env python3

import os
import sys

# TODO: Determine which directory to walk from command line argument

if len(sys.argv) == 2:
    directory = str(sys.argv[1])
else:
    directory = os.getcwd()

# TODO: Walk specified directory in sorted order and print out each entry's
# file name

dirScan = os.scandir(directory)
output = []

for i in dirScan:
    output.append(i.name)
for i in sorted(output):
    print(i)
