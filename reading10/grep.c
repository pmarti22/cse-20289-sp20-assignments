/* cat.c */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Globals */

char * PROGRAM_NAME = NULL;

/* Functions */

void usage(int status) {
    fprintf(stderr, "Usage: %s PATTERN\n", PROGRAM_NAME);
    exit(status);
}

bool cat_stream(FILE *stream, char *pattern) {
    char buffer[BUFSIZ];

    int count = 0;

    while (fgets(buffer, BUFSIZ, stream)) {
      if(strstr(buffer, pattern)){
        fputs(buffer, stdout);
        count++;
      }
    }

    if(count == 0){
      return false;
    }

    return true;
}

/* Main Execution */

int main(int argc, char *argv[]) {
    int argind = 1;

    /* Parse command line arguments */
    PROGRAM_NAME = argv[0];

    if (argc == 1){
      usage(0);
    }

    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
      char *arg = argv[argind++];
      if (strcmp(arg, "-h") == 0) {
        usage(0);
      } else {
        usage(1);
      }
    }

    char *pattern = argv[1];

    /* Process each file */
    return !cat_stream(stdin, pattern);
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
